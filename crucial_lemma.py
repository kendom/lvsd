from typing import List
from abc import ABC, abstractmethod
class Edit(ABC):
    @abstractmethod
    def act(self, string) -> str:
        return ""

class Insertion(Edit):
    def __init__(self, index, char):
        self.index = index
        self.char = char

    def act(self, string):
        index = self.index
        if index < 0 or index > len(string):
            raise RuntimeError("Invalid edit.")
        elif index == 0:
            return self.char+string
        elif index == len(string):
            return string+self.char
        else:
            return string[0:index]+self.char+string[index:]

class Deletion(Edit):
    def __init__(self, index):
        self.index = index

    def act(self, string):
        index = self.index
        if index < 0 or index >= len(string):
            raise RuntimeError("Invalid edit.")
        elif index == 0:
            return string[1:]
        elif index == len(string)-1:
            return string[0:len(string)-1]
        else:
            return string[0:index] + string[index+1:]

class Modification(Edit):
    def __init__(self, index, char):
        self.index = index
        self.char = char

    def act(self, string):
        index = self.index
        if index < 0 or index >= len(string):
            raise RuntimeError("Invalid edit.")
        elif index == 0:
            return self.char+string[1:]
        elif index == len(string)-1:
            return string[0:len(string)-1]+self.char
        else:
            return string[0:index]+self.char+string[index+1:]

class EditSequence():
    def __init__(self, X:str, Y:str, edits: List[Edit]):
        cur = X
        for ed in edits:
            try:
                cur = ed.act(cur)
            except RuntimeError:
                raise RuntimeError("Some edit is invalid.")
        if not cur == Y:
            raise RuntimeError("X is not mapped to Y by the sequence.")
        self.X = X
        self.Y = Y
        self.edits = edits
        self.Tmat = None
        self.lr = None

    def partials(self, n):
        if n > len(self.edits):
            raise RuntimeError("Invalid index.")
        cur = self.X
        res = [cur]
        for ix in range(0,n):
            cur = self.edits[ix].act(cur)
            res.append(cur)
        return res

    def create_Tmat(self):
        Tmat = []
        for c in self.X:
            Tmat.append([c])
        for ed in self.edits:
            #Getting non-deleted columns.
            on_columns = [ix for ix in range(len(Tmat)) if Tmat[ix][-1] != '!']
            #Now, `Deletion(jx)` corresponds to acting on `Tmat[on_columns[jx]]`.
            if isinstance(ed, Insertion):
                #There are two cases -- need to add a new column, or can use an old one.
                if len(on_columns) == 0: 
                    #Everything is marked deleted.
                    ins_col = 0
                elif ed.index == len(on_columns): 
                    #Insert at end (of the undeleted).
                    ins_col = on_columns[-1] + 1
                else: 
                    #Insert not at end (of the undeleted).
                    ins_col = on_columns[ed.index]
                if ins_col == 0:
                    if Tmat[0][-1] == "!":
                        Tmat[0].append(ed.char)
                    else:
                        Tmat.insert(0, [' ', ed.char])
                elif ins_col == len(Tmat):
                    Tmat.append([' ', ed.char])
                else:
                    if Tmat[ins_col][-1] == '!':
                        Tmat[ins_col].append(ed.char)
                    elif Tmat[ins_col-1][-1] == '!':
                        Tmat[ins_col-1].append(ed.char)
                    else:
                        Tmat.insert(ins_col, [' ', ed.char])
            elif isinstance(ed, Modification):
                Tmat[on_columns[ed.index]].append(ed.char)
            elif isinstance(ed, Deletion):
                Tmat[on_columns[ed.index]].append("!")
        self.Tmat = Tmat
        return Tmat

    def pretty_print_Tmat(self):
        if self.Tmat is None:
            self.create_Tmat()
        Tmat = self.Tmat
        max_rows = max([len(col) for col in Tmat])
        output = ""
        for row in range(0, max_rows):
            for col in range(0,len(Tmat)):
                if row < len(Tmat[col]):
                    output += Tmat[col][row]
                else:
                    output += " "
            output += "\n"
        return output

    def to_lr(self):
        if self.Tmat is None:
            self.create_Tmat()
        Tmat = self.Tmat
        lr_edits = []
        pos = 0
        #Read off the Tmat from top to bottom, left to right.
        for col in Tmat:
            #A move is a pair (col[ix], col[ix+1]).
            for ix in range(0,len(col)-1):
                if col[ix] != "!" and col[ix+1] == "!":
                    lr_edits.append(Deletion(pos))
                elif col[ix] == "!" and col[ix+1] != "!":
                    lr_edits.append(Insertion(pos, col[ix+1]))
                elif col[ix] == " ":
                    lr_edits.append(Insertion(pos, col[ix+1]))
                else:
                    lr_edits.append(Modification(pos, col[ix+1]))
            pos += 0 if col[-1] == "!" else 1
        self.lr = EditSequence(self.X, self.Y, lr_edits)
        return self.lr

X = "xbzy"
Y = "ab"
d1 = Deletion(2)
d2 = Deletion(0)
d3 = Deletion(1)
i1 = Insertion(1, 'c')
d4 = Deletion(1)
i2 = Insertion(0,'a')
es1 = EditSequence(X, Y, [d1,d2,d3,i1,d4,i2])

dd1 = Deletion(0)
ii1 = Insertion(1, 'c')
dd2 = Deletion(1)
ii2 = Insertion(1, 'y')
ii3 = Insertion(0, 'x')
ii4 = Insertion(2, 'z')
es2 = EditSequence(Y, X, [dd1, ii1, dd2, ii2, ii3, ii4])

m1 = Modification(0, 'x')
iii1 = Insertion(2, 'z')
iii2 = Insertion(3, 'y')
es3 = EditSequence(Y, X, [m1, iii1, iii2])

print(es1.pretty_print_Tmat())
print(es2.pretty_print_Tmat())
print(es3.pretty_print_Tmat())

A = "abc"
B = "tobias"
mo1 = Modification(0, "t")
mo2 = Modification(1, "o")
mo3 = Modification(2, "b")
in1 = Insertion(3, "i")
in2 = Insertion(4, "a")
in3 = Insertion(5, "s")
es4 = EditSequence(A, B, [mo1, mo2, mo3, in1, in2, in3])

print(es4.pretty_print_Tmat())
